/*
 * Configuration
 */

var conf = require("./conf.priv");


/*
 * Modules
 */

var m_irc = require("irc"),
    m_http = require("http"),
    m_express = require("express"),
    m_feed = require("feed-read"),
    m_entities = require("entities"),
    m_oauth = require("oauth"),
    m_exec = require("child_process").exec,
    m_fs = require("fs");


/*
 * IRC client
 */

var irc = new m_irc.Client(conf.irc.host, conf.irc.nick, {
        channels: conf.irc.chans,
        floodProtection: true,
        floodProtectionDelay: 1000
    }),
    commands = {};

m_fs.readFile(conf.cmd.list, "utf8", function(error, data) {
    if (error)
    {
        console.log("Error:", error);
        return;
    }
    cmd = JSON.parse(data).commands;
});

irc.addListener("error", function(message) {
    console.log("Error:", message);
});

irc.addListener("message", function(from, to, message) {
    console.log(from + " -> " + to + ": " + message);

    if (message.indexOf("!cmd ") == 0)
    {
        var raw_cmd = message.replace(/\!cmd\s/, ""),
            shell_cmd;

        for (var i = 0; i < cmd.length; i++)
        {
            if (cmd[i].irc == raw_cmd)
            {
                shell_cmd = cmd[i].cmd;
                break;
            }
        }

        if (shell_cmd)
        {
            irc.say(to, m_irc.colors.wrap("dark_green", "Executing command: ") +
                m_irc.colors.wrap("orange", raw_cmd));

            m_exec(shell_cmd, function(error, stdout, stderr) {
                var file = from + "-" +
                        new Date().toString()
                            .replace(/[^a-z0-9]+/gi, "-")
                            .replace(/\-$/, "") +
                        ".log",
                    contents = "Command: " + shell_cmd + "\n\n" + stderr + "\n" + stdout;

                m_fs.writeFile("scripts_logs/" + file, contents, function(error) {
                    if (error)
                    {
                        console.log(error);
                        return;
                    }

                    var url = "http://" + conf.http.host + ":" + conf.http.port + "/scripts/" + file,
                        req = m_http.request({
                            hostname: "url.mursa.at",
                            path: "/makeurl?url=" + encodeURIComponent(url)
                        }, function(result) {
                            result.on("data", function(chunk) {
                                chunk = JSON.parse(chunk).url;
                                irc.say(to, m_irc.colors.wrap("dark_green", "Command: ") +
                                    m_irc.colors.wrap("orange", raw_cmd) + " > " +
                                    m_irc.colors.wrap("dark_red", chunk));
                                });
                        });

                    req.end();
                });
            });
        }
    }
    else if (message.indexOf("!join ") == 0)
    {
        var chan = message.replace(/\!join\s/, "");
        irc.join(chan);
    }
    else if (message.indexOf("!part") == 0)
        irc.part(to);

    var urls = message.match(/((http|https|ftp)\:\/\/(\S*?\.\S*?))(\s|\)|\]|\[|\{|\}|(,\s)|(,<)|"|\'|:|\<|$|\.\s|\.<)/gi);
    if (urls)
    {
        for (var i = 0; i < urls.length; i++)
        {
            var req = m_http.request({
                    hostname: "url.mursa.at",
                    path: "/makeurl?url=" + encodeURIComponent(urls[i])
                }, function(result) {
                    result.on("data", function(chunk) {
                        chunk = JSON.parse(chunk).url;
                        irc.say(to, m_irc.colors.wrap("magenta", chunk));
                    });
                });

            req.end();
        }
    }

    var quoi = message.match(/^quoi[\s\?\!]*$/i);
    if (quoi)
        irc.say(to, from + ": ffeur");
});


/*
 * HTTP server
 */

var http_serv = m_express();
http_serv.use(m_express.json());
http_serv.use(m_express.urlencoded());

http_serv.post("/git/*", function(request, response) {
    if (request.body.payload)
    {
        var git = JSON.parse(request.body.payload),
            commit = git.commits[0],
            repo = git.repository,
            msg = commit.message.replace(/\n/g, ""),
            url = "https://bitbucket.org/" + repo.owner + "/" + repo.slug + "/changeset/" + commit.node,
            send = "[" + m_irc.colors.wrap("dark_blue", repo.slug) +
                "/" + m_irc.colors.wrap("dark_blue", commit.branch) + "] " +
                m_irc.colors.wrap("dark_green", commit.node) + " - " +
                m_irc.colors.wrap("magenta", commit.author) + ": « " +
                m_irc.colors.wrap("orange", msg) + " » ";

        var req = m_http.request({
                hostname: "url.mursa.at",
                path: "/makeurl?url=" + encodeURIComponent(url)
            }, function(result) {
                result.on("data", function(chunk) {
                    chunk = JSON.parse(chunk).url;
                    send += "(" + m_irc.colors.wrap("dark_red", chunk) + ")";
                    irc.say("#" + request.url.replace("/git/", ""), send);
                });
            });

        req.end();
    }

    response.end();
});

http_serv.get("/scripts/*.log", function(request, response) {
    var file = "scripts_logs/" + request.url.replace("/scripts/", ""),
        output;

    response.set("Content-Type", "text/plain");
    m_fs.readFile(file, "utf8", function(error, data) {
        if (error)
            output = "Error " + error.errno + ": " + error.code + "\nFile: " + error.path + "\n";
        else
            output = data;

        response.send(output);
    });
});

http_serv.listen(conf.http.port);
console.log("HTTP server started on port", conf.http.port);


/*
 * Dropbox
 */

var dbox_last = false;

setInterval(function() {
    m_feed(conf.dropbox.feed, function(error, articles) {
        if (error)
        {
            console.log("Error reading dropbox feed");
            return;
        }

        for (var i = articles.length - 1; i >= 0; i--)
            if (articles[i].published == dbox_last)
                break;

        dbox_last = articles[0].published.toString();

        if (i == -1 || articles[i].published == dbox_last)
            return;

        for (i--; i >= 0; i--)
        {
            var msg = (m_irc.colors.wrap("dark_red", "Dropbox") + ": " + articles[i].content)
                    .replace(/<a[^>]*>([^<]+)<\/a>/g, m_irc.colors.wrap("orange", "$1"))
                    .replace(/<[^>]*>/g, "")
                    .replace(/[\s]+/, " ");

            irc.say(conf.dropbox.chan, m_entities.decode(msg));
        }
    })
}, 10000);


/*
 * Twitter
 */

var twitter = new m_oauth.OAuth(
        "https://api.twitter.com/oauth/request_token",
        "https://api.twitter.com/oauth/access_token",
        conf.twitter.consumerKey,
        conf.twitter.consumerSecret,
        '1.0A',
        null,
        'HMAC-SHA1'
    );

var tweet_last = false;

setInterval(function() {
    twitter.get(
        "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=" +
            conf.twitter.screenName + "&count=" + conf.twitter.contPosts,
        conf.twitter.userToken,
        conf.twitter.userSecret,
        function (e, data, res) {
            if (e != null)
            {
                console.log("Twitter error:", e);
                return;
            }

            var tweets = [];

            try {
                tweets = JSON.parse(data);
            }
            finally {
                if (tweets.length == 0)
                    return;

                for (var i = tweets.length - 1; i >= 0; i--)
                    if (tweets[i].id_str == tweet_last)
                        break;

                tweet_last = tweets[0].id_str.toString();

                if (i == -1 || tweets[i].id_str == tweet_last)
                    return;

                for (i--; i >= 0; i--)
                {
                    var send = tweets[i].text
                        .replace(/(\@[^\s\:\.]+)/g, m_irc.colors.wrap("cyan", "$1"))
                        .replace(/(\#[^\s\:\.]+)/g, m_irc.colors.wrap("orange", "$1"))
                        .replace(/(http\:\/\/t\.co\/[a-z0-9]+)/gi, m_irc.colors.wrap("dark_red", "$1"))
                        .replace(/\s+/g, " ");
                    irc.say(conf.twitter.chan,
                        m_irc.colors.wrap("dark_green", "@" + tweets[i].user.screen_name) +
                        ": " + m_entities.decode(send));
                }
            }
        }
    );
}, 10000);

