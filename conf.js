/*
 * Global configuration
 */

module.exports = {
        irc: {
            nick: "Emma",
            host: "irc.u-psud.rezosup.org",
            chans: [ "#mursaat", "#gw2js" ]
        },
        cmd: {
            list: __dirname + "/commands.json"
        },
        http: {
            host: "dev.yoone.eu",
            port: 6977
        },
        twitter: {
            consumerKey: null, // priv
            consumerSecret: null, // priv
            userToken: null, // priv
            userSecret: null, // priv
            screenName: "GW2_FR",
            countPosts: 5,
            chan: "#mursaat"
        },
        dropbox: {
            feed: null, // priv
            chan: "#mursaat"
        }
    };

