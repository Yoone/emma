#!/bin/sh

path=/var/www/gw2.fr/v6.dev

rm -rf ${path}/app/cache/*
php ${path}/app/console cache:clear --env=prod
